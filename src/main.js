var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var steps = 24
var maxSize = 0.666

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < steps; i++) {
    fill(255, 255, 255, 0.25 * (i / steps))
    noStroke()
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    rotate(frameCount * 0.01 + i * 0.5 * tan(i * 0.01 + frameCount * 0.001))
    rect(0, 0, boardSize  * maxSize * (1 - (i / steps)), boardSize  * maxSize * (1 - (i / steps)))
    pop()
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
